﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Thoughts

// what happens if we get a log with the same time?
    // assuming we will not have a log file with conflicting info (different states for same time)
// we should add the times to a dictionary and then once they are all added, then go over them
// in order to iterate over the keys, we will need to sort the keys
// will this work for multi threading - in multi threading you have you multiple threads all adding to the same dictionary
    // this means that you have some threads inserting while one is going over the results - we might miss something
    // once we have a list of keys, then iterate over the dictionary - this allows some threads to insert while we are going over the dictionary
    // we want to make sure the latest thread coming in is the one that returns the final result
    // what happens if threads are constantly coming in and there are always threads running?
    // once a thread finished, no threads that started earlier should be allowed to update the flaw count
    // need to make sure than an earlier thread is finished adding entries before a more recent thread can finish
// We cannot remove extra "heartbeat times" in case we get a new time in between
// Disposing of the stream should be the caller's responsibility

namespace RC.CodingChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            IEventCounter eventCounter = new HVACFlawCounter();

            List<Tuple<string, int>> eventIsolatedLogs = GetIsolatedEventLogs();
            List<Tuple<string, int>> eventMultipleLogs = GetConcurrentEventLogs();
            Console.WriteLine("Testing isolated events");
            TestEvents(eventIsolatedLogs);
            Console.WriteLine("Testing concurrent events");
            TestEvents(eventMultipleLogs, eventCounter);

            Console.WriteLine("Press any key to close");
            Console.ReadLine();
        }

        /// <summary>
        /// A method to test event logs and their expected outcome
        /// </summary>
        /// <param name="eventLogs"></param>
        /// <param name="eventCounter"></param>
        static void TestEvents(List<Tuple<string, int>> eventLogs, IEventCounter eventCounter = null)
        {
            bool createEventCounter = eventCounter == null;
            eventLogs.ForEach(e =>
            {
                string eventLog = e.Item1;
                int expected = e.Item2;

                if (createEventCounter)
                {
                    eventCounter = new HVACFlawCounter();
                }

                byte[] byteArray = Encoding.UTF8.GetBytes(eventLog);

                using (MemoryStream stream = new MemoryStream(byteArray))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        eventCounter.ParseEvents("HVAC1", reader);
                        int count = eventCounter.GetEventCount("HVAC1");
                        Console.WriteLine(String.Format("Expected: {0} \t Calculated: {1}", expected, count));
                    };
                }
            });
        }

        /// <summary>
        /// Returns a list of event logs where the union of all results
        /// yields a different number of flaws than each individually
        /// </summary>
        /// <returns></returns>
        static List<Tuple<string, int>> GetConcurrentEventLogs()
        {
            List<Tuple<string, int>> tuples = new List<Tuple<string, int>>();

            string event1 = @"
                2017-08-31 22:53:45	1
                2017-08-31 22:54:45	3
                2017-08-31 22:59:47	0
                2017-08-31 23:00:45	3
                2017-08-31 23:01:45	3
                2017-08-31 23:07:45	2
                2017-08-31 23:08:45	3
                2017-08-31 23:11:40	2
                2017-08-31 23:14:45	0
                2017-08-31 23:15:45	3
                2017-08-31 23:17:45	3
                2017-08-31 23:17:55	3
                2017-08-31 23:18:45	3
                2017-08-31 23:24:45	2
                2017-08-31 23:26:45	2
                2017-08-31 23:28:45	0
                2017-08-31 23:30:45	3
                2017-08-31 23:35:45	2
                2017-08-31 23:40:45	0
                2017-08-31 23:50:45	2";
            event1 = event1.Replace("                ", "").Replace("\r", "");
            tuples.Add(Tuple.Create(event1, 3));

            string event2 = @"
                2017-08-31 23:04:45	1";
            event2 = event2.Replace("                ", "").Replace("\r", "");
            tuples.Add(Tuple.Create(event2, 2));

            return tuples;
        }

        /// <summary>
        /// Returns a list of event logs where each is treated individually
        /// </summary>
        /// <returns></returns>
        static List<Tuple<string, int>> GetIsolatedEventLogs()
        {
            // Should test the following sequences
            // event1: 3,2,0,1,1 (long 3)
            // event1: 3,2,3,0,1 (both 3s are long)
            // event1: 3,3,2,0,1 (2 short 3s = long 3)
            // event2: 1,1,1,1,1 (no 3)
            // event3: 3,0,1,1,1 (no 3 followed by 2)
            // event4: 3,2,0,1,1 (not long 3)
            // event5: 3,2,3,0,1 (long 3)
            List<Tuple<string, int>> tuples = new List<Tuple<string, int>>();

            string event1 = @"
                2017-08-31 22:53:45	1
                2017-08-31 22:54:45	3
                2017-08-31 22:59:47	0
                2017-08-31 23:00:45	3
                2017-08-31 23:01:45	3
                2017-08-31 23:07:45	2
                2017-08-31 23:08:45	3
                2017-08-31 23:14:40	2
                2017-08-31 23:14:45	0
                2017-08-31 23:15:45	3
                2017-08-31 23:17:45	3
                2017-08-31 23:17:55	3
                2017-08-31 23:18:45	3
                2017-08-31 23:24:45	2
                2017-08-31 23:26:45	2
                2017-08-31 23:28:45	0
                2017-08-31 23:30:45	3
                2017-08-31 23:35:45	2
                2017-08-31 23:40:45	0
                2017-08-31 23:50:45	2";
            event1 = event1.Replace("                ", "").Replace("\r", "");
            tuples.Add(Tuple.Create(event1, 3));

            string event2 = @"
                2017-08-31 22:53:45	1
                2017-08-31 22:54:45	1
                2017-08-31 22:59:47	0
                2017-08-31 23:00:45	2
                2017-08-31 23:01:45	2
                2017-08-31 23:07:45	2
                2017-08-31 23:15:45	0";
            event2 = event2.Replace("                ", "").Replace("\r", "");
            tuples.Add(Tuple.Create(event2, 0));

            string event3 = @"
                2017-08-31 22:53:45	1
                2017-08-31 22:54:45	3
                2017-08-31 22:59:47	0
                2017-08-31 23:00:45	2
                2017-08-31 23:01:45	2
                2017-08-31 23:10:45	3
                2017-08-31 23:14:45	0
                2017-08-31 23:15:45	0";
            event3 = event3.Replace("                ", "").Replace("\r", "");
            tuples.Add(Tuple.Create(event3, 0));

            string event4 = @"
                2017-08-31 22:53:45	3
                2017-08-31 22:54:45	2
                2017-08-31 22:59:47	0";
            event4 = event4.Replace("                ", "").Replace("\r", "");
            tuples.Add(Tuple.Create(event4, 0));

            string event5 = @"
                2017-08-31 22:48:45	3
                2017-08-31 22:54:45	2
                2017-08-31 22:54:47	3
                2017-08-31 22:59:49	0";
            event5 = event5.Replace("                ", "").Replace("\r", "");
            tuples.Add(Tuple.Create(event4, 0));

            return tuples;
        }

        /// <summary>
        /// Can be used to create fake event logs
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        static string CreateEventLog(int size)
        {
            Random rand = new Random();
            DateTime startTime = DateTime.Now.AddHours(-1);
            int differenceMinutes = (int)(DateTime.Now - startTime).TotalMinutes;

            List<DateTime> randomTimes = new List<DateTime>();
            for (int i = 0; i < size; i++)
            {
                int randMinutes = rand.Next(0, differenceMinutes);
                DateTime newTime = startTime.AddMinutes(randMinutes);
                randomTimes.Add(newTime);
            }

            randomTimes.Sort();
            List<string> eventStrings = randomTimes.Select(t =>
            {
                int randState = rand.Next(0, 4);
                string eventRecord = String.Format("{0}\t{1}", t.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture), randState);
                return eventRecord;
            }).ToList();

            string events = String.Join("\n", eventStrings);
            return events;
        }
    }
}

