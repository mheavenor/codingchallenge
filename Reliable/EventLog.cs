﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RC.CodingChallenge
{
    public class EventLog
    {
        public ConcurrentDictionary<string, string> Events { get; set; }

        public EventLog()
        {
            Events = new ConcurrentDictionary<string, string>();
        }

        /// <summary>
        /// A method that takes in multiple events as a single string, delmimited by \n,
        /// and adds them to the event log.
        /// The event is a time and a state separated by a tab
        /// </summary>
        /// <param name="eventString">The event string. Usually output from stream.ReadToEnd()</param>
        public void AddEvents(string eventString)
        {
            List<string> events = eventString.Split('\n').ToList();
            events.ForEach(e =>
            {
                // We dont want to catch a newline by itself
                if (String.IsNullOrWhiteSpace(e))
                {
                    return;
                }
                
                // We are asumming that this is well formed - as from the requirements
                List<string> eventRecord = e.Split('\t').ToList();
                string timestamp = eventRecord[0];
                string state = eventRecord[1];

                Events.AddOrUpdate(timestamp, state, (key, value) => state);
            });
        }
    }
}
