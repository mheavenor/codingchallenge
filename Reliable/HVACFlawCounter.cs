﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RC.CodingChallenge
{
    public class HVACFlawCounter : IEventCounter
    {
        public ConcurrentDictionary<string, EventLog> EventDict { get; set; }

        public ConcurrentDictionary<string, int> FlawsDict { get; set; }

        private ConcurrentDictionary<string, ConcurrentDictionary<int, List<int>>> PreviousDeviceThreadIdsDict { get; set; }

        public Semaphore Semaphore { get; set; }

        public HVACFlawCounter()
        {
            EventDict = new ConcurrentDictionary<string, EventLog>();
            FlawsDict = new ConcurrentDictionary<string, int>();
            PreviousDeviceThreadIdsDict = new ConcurrentDictionary<string, ConcurrentDictionary<int, List<int>>>();
            Semaphore = new Semaphore(1, 1);
        }

        public int GetEventCount(string deviceID)
        {
            if (FlawsDict.ContainsKey(deviceID))
            {
                return FlawsDict[deviceID];
            } else
            {
                return -1;
            }
        }

        public void ParseEvents(string deviceID, StreamReader eventStream)
        {
            // Threading thoughts

            // We need to make sure all threads are referencing the same event log.
            // The first "if" statement, which handles the initialization of the event 
            // log, should only be run by one thread at a time.
            // We dont want to lock before the if statement everytime, thats expensive,
            // we could just lock inside the if statement and then have one more identical "if"
            // inside for any threads that snuck through the first "if"

            // If there are two threads, we need to make sure that one thread can't be the first
            // to add entries, and the last to update flawsNum
            // As soon as a thread updates the flawsNum, no threads that added entries before
            // are allowed to update flawsNum

            if (!EventDict.ContainsKey(deviceID))
            {
                Semaphore.WaitOne();
                if (!EventDict.ContainsKey(deviceID))
                {
                    EventLog newEventLog = new EventLog();
                    EventDict.GetOrAdd(deviceID, newEventLog);
                    FlawsDict.GetOrAdd(deviceID, 0);
                    PreviousDeviceThreadIdsDict.GetOrAdd(deviceID, new ConcurrentDictionary<int, List<int>>());
                }
                Semaphore.Release(1);
            }

            EventLog eventLog = EventDict[deviceID];
            ConcurrentDictionary<int, List<int>> previousThreadIdsDict = PreviousDeviceThreadIdsDict[deviceID];

            string eventString = eventStream.ReadToEnd();
            eventLog.AddEvents(eventString);

            // Grab all threads that are currently working for the current device
            Semaphore.WaitOne();
            List<int> pastThreadIds = previousThreadIdsDict.Keys.ToList();
            int currentThreadId = Thread.CurrentThread.ManagedThreadId;
            previousThreadIdsDict[currentThreadId] = pastThreadIds;
            Semaphore.Release(1);

            List<string> times = eventLog.Events.Keys.ToList();
            int flawsNum = CountFlaws(eventLog, times);

            // Check if the current thread is allowed to make updates
            // Only allowed to make updates if they are still in the dict
            Semaphore.WaitOne();
            if (previousThreadIdsDict.ContainsKey(currentThreadId))
            {
                FlawsDict[deviceID] = flawsNum;
                List<int> earlierThreads;
                // Remove the thread from the dict
                previousThreadIdsDict.TryRemove(currentThreadId, out earlierThreads);

                // Remove all previous threads from the dict
                earlierThreads.ForEach(t =>
                {
                    List<int> threads;
                    previousThreadIdsDict.TryRemove(t, out threads);
                });
            }
            Semaphore.Release(1);
        }

        /// <summary>
        /// A method to count the flaws in the event logs
        /// </summary>
        /// <param name="eventLog">The event log</param>
        /// <param name="timeStrings">A list of strings of times (format yyyy-MM-dd HH:mm:ss) and states separated by a tab</param>
        /// <returns>The number of flaws</returns>
        private int CountFlaws(EventLog eventLog, List<string> timeStrings)
        {
            // We need to find flaws
            // Flaws are defined by finding a 3 for >= 5 minutes
            // Followed by an immediate 2
            // Followed by any combination of 3 or 2
            // Followed by a 0

            int flawsCount = 0;
            if (timeStrings.Count <= 2)
            {
                return flawsCount;
            }

            HVACFlawStep step = HVACFlawStep.Nothing;
            timeStrings.Sort();
            
            // We know the time format will not deviate from this
            string timeFormat = "yyyy-MM-dd HH:mm:ss";

            // Set the initial time
            string previousTimeString = timeStrings[0];
            string previousState = eventLog.Events[timeStrings[0]];
            timeStrings.RemoveAt(0);

            timeStrings.ForEach(t =>
            {
                string currentState = eventLog.Events[t];
                // Check if we have been on state "3" for longer than 5 minutes
                if (step == HVACFlawStep.Nothing && previousState == "3")
                {
                    DateTime previousTime = DateTime.ParseExact(previousTimeString, timeFormat, CultureInfo.InvariantCulture);
                    DateTime currentTime = DateTime.ParseExact(t, timeFormat, CultureInfo.InvariantCulture);
                    double diffMinutes = (currentTime - previousTime).TotalMinutes;
                    if (diffMinutes >= 5)
                    {
                        step = HVACFlawStep.Step1;
                    }
                }

                // Check if state "2" immediately follows a long step "3"
                if (step == HVACFlawStep.Step1)
                {
                    if (currentState == "2")
                    {
                        step = HVACFlawStep.Step2;
                    }
                    else
                    {
                        step = HVACFlawStep.Nothing;
                    }
                }
                // Check if state "0" (flaw) or states "2" or "3" (continue)
                else if (step == HVACFlawStep.Step2)
                {
                    if (currentState == "0")
                    {
                        step = HVACFlawStep.Step3;
                    } else if (currentState == "1")
                    {
                        step = HVACFlawStep.Nothing;
                    }
                }

                if (currentState != previousState)
                {
                    previousState = currentState;
                    previousTimeString = t;
                }

                if (step == HVACFlawStep.Step3)
                {
                    flawsCount++;
                    step = HVACFlawStep.Nothing;
                }
            });

            return flawsCount;
        }
    }
}

